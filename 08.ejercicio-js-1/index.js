const URL = 'https://randomuser.me/api/?results=';

let usersArray = [];

async function getUsers(cant) {
  try {
    const response = await fetch(`${URL}+${cant}`);
    const data = await response.json();
    const users = data.results;

    for (const user of users) {
      const User = {
        username: `${user.login.username}`,
        fullname: `${user.name.first} ${user.name.last}`,
        sex: `${user.gender}`,
        country: `${user.location.country}`,
        email: `${user.email}`,
        picture: `${user.picture.large}`,
      };

      usersArray.push(User);
    }

    console.log(usersArray);
  } catch (error) {
    console.log(error);
  }
}

getUsers(prompt('Escriba la cantidad de Usuarios que quiere ver: '));
// getUsers(5);
