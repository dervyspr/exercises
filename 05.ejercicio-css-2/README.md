# Selectores II

Recuerdas el formulario que tuviste que hacer en el Ejercicio 3 de HTML? Pues añadele un CSS para que se represente de esta forma:

- Haz que el formulario siempre esté centrado en la pantalla y que tenga un ancho máximo de 800px y un ancho mínimo de 400px.
- Haz que todos los campos obligatorios aparezcan con un borde rojo. -- ok

- Haz que todos los fieldsets tengan fondo gris claro y un padding de `1rem`. ---OK

- Haz que al pasar por encima el ratón de los fieldsets cambien de color de fondo a un gris más oscuro. -- ok

- Haz que el primer y último fieldset tengan un borde inferior de color negro y de 5 píxeles de ancho. --- ok

- Haz que el botón de envío tenga fondo negro, texto blanco y no tenga borde. ------ ok

El CSS resultante debe ser validado por el [validador de CSS de la W3](https://jigsaw.w3.org/css-validator/#validate_by_input) y no dar ningún error.
