function conversor(num, base) {
  if (base === 10) {
    console.log(num.toString(2));
  } else if (base === 2) {
    let sum = 0;
    let numReverse = num.toString().split('').reverse().join('');
    console.log(numReverse);

    for (index = 0; index < numReverse.length; index++) {
      sum = sum + numReverse[index] * 2 ** index;
    }

    console.log(sum);
  } else {
    console.log(
      `${base} no es una base correcta. Escriba Decimal(10) - Binario(2)`
    );
  }
}

conversor(30, 10);

// function BinarioADecimal(num) {

//   return sum;
// }

// console.log(BinarioADecimal('11110'));
